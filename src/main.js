import Vue from 'vue';
import App from './App.vue';
import router from '@/router';
import Vuex from 'vuex';
import {
    store
} from './store/store';
Vue.config.productionTip = false;
new Vue({
    store: store,
    el: '#app',
    router,
    render: h => h(App),
});