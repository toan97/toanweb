import VueRouter from "vue-router";
import Vue from 'vue';
import Layout from "@/layouts/default.vue";
import Index from "@/pages/index.vue";
import Post from "@/pages/post.vue";
import Introduct from "@/pages/introduct.vue";
import Contact from "@/pages/contact.vue";
import Cart from "@/pages/cart.vue";
import Checkout from "@/pages/priview.vue";
import Vuex from "vuex";
Vue.use(VueRouter)

export default new VueRouter({
    mode: "history",
    routes: [{
        path: '/',
        name: 'Layout',
        component: Layout,
        children: [{
                path: "/",
                name: "Index",
                component: Index

            },
            {
                // path: "/post/:slug",
                path: "/post/:dfsdfsdf",
                name: "Post",
                component: Post
            },
            // gioi thieu
            {
                path: '/introduct',
                name: "Introduct",
                component: Introduct
            },
            {
                path: "/contact",
                name: "Contact",
                component: Contact
            },
            {
                path: "/cart",
                name: "Cart ",
                component: Cart
            },
            {
                path: "/priview",
                name: "Priview",
                component: Checkout
            }
        ]
    }]
})