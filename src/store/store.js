import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
export const store = new Vuex.Store({
    strict: true,
    state: {
        products: [{
                id: 1,
                name: "Annie Pinafore",
                price: 64.0103,
                image: '/assets/images/catalog/annie_pinafore_4_384x384.jpg'
            },
            {
                id: 2,
                name: "Orla Slip Dress",
                price: 55.00,
                image: '/assets/images/catalog/orla_slip_dress_in_coral_washed_linen_3_a26e143f-6bc1-45cb-9c40-42aa2b405980_384x384.jpg'
            },
            {
                id: 3,
                name: "Halter Tank",
                price: 44.0,
                image: '/assets/images/catalog/halter_tank_in_cream_gauze_2_af3e4b94-f7ca-4c51-8528-e90a3237f6b0_384x384.jpg'
            },
            {
                id: 4,
                name: "Mabel Playsuit",
                price: 64.0,
                image: '/assets/images/catalog/mabel_playsuit_in_citron_washed_linen_4_b103dc04-c21f-453a-9cb3-d70cccec6197_384x480.jpg'
            },
            {
                id: 5,
                name: "Slouchy Bubble Shorts",
                price: 64.0,
                image: '/assets/images/catalog/james_beach_pants_big_blue_check_cotton_2_d817be06-a66f-4bcc-83ba-6e7e3793f5b1_384x480.jpg'
            },
            {
                id: 6,
                name: "Chloe Ruffle Sleeve Dress",
                price: 70.0,
                image: '/assets/images/catalog/chloe_384x480.jpg'
            },
            {
                id: 7,
                name: "Frankie Overall Shorts",
                price: 64.0,
                image: '/assets/images/catalog/frankie_overall_shorts_1_384x384.jpg'
            },
            {
                id: 8,
                name: "James Beach Pants",
                price: 50.0,
                image: '/assets/images/catalog/james_beach_pants_big_blue_check_cotton_2_d817be06-a66f-4bcc-83ba-6e7e3793f5b1_384x480.jpg'
            },
            {
                id: 9,
                name: "Nona Dress",
                price: 70.0,
                image: '/assets/images/catalog/nona_dress_in_liberty_of_london_2_ca8fa4cd-2a67-410a-bd7e-777cc015e2bc_384x480.jpg'
            },
            {
                id: 10,
                name: "Mary Wrap Pinafore",
                price: 64.0,
                image: '/assets/images/catalog/mary_wrap_pinafore_in_citron_washed_linen_4_6db49e8a-d374-4180-8ee2-5fb9fc0a9f92_384x384.jpg'
            },
            {
                id: 11,
                name: "Edith Blouse",
                price: 48.0,
                image: '/assets/images/catalog/edith_blouse_1_384x384.jpg'
            },
            {
                id: 12,
                name: "Gemma Dress",
                price: 77.0,
                image: '/assets/images/catalog/gemma_dress_in_clover_green_gauze_3_384x480.jpg'
            },
            {
                id: 13,
                name: "James track Shorts",
                price: 48.0,
                image: '/assets/images/catalog/james_track_shorts_in_clover_washed_linen_2_384x384.jpg'
            },
            {
                id: 14,
                name: "Pip Romper",
                price: 68.0,
                image: '/assets/images/catalog/pip_romper_in_big_blue_white_check_3_384x480.jpg'
            },
            {
                id: 15,
                name: "Frances Beach Skirt",
                price: 70.0,
                image: '/assets/images/catalog/frances_beach_skirt_1_384x480.jpg'
            },

        ],
        carts: [],
        cart_ids: [],
    },
    mutations: {
        addToCart(state, data) {
            // mutate state
            var index_cart = state.cart_ids.indexOf(data.id);
            if (index_cart !== -1) {
                state.carts[index_cart].count++;
            }
            data.count = 1;
            state.carts.push(data);
            state.cart_ids.push(data.id);
            console.log(state.carts);
        },
        removeCart(state, product_id) {
            var index_cart = state.cart_ids.indexOf(product_id);
            var index_cart = state.cart_ids.indexOf(product_id);
            console.log(state.carts);
            var cart_object = state.carts;
            if (index_cart !== -1) {
                cart_object.splice(index_cart, 1);
                state.cart_ids.splice(index_cart, 1);
                state.carts = cart_object;
            }
        }
    }
})